import random


def input_num():
    while True:
        try:
            input_num = int(input('Введите число от 1 до 100. => '))
        except ValueError as e:
            print('Введите число!')
        except TypeError:
            print('Введите число =(')
        else:
            return input_num


def main():
    err_try = 10
    random_num = random.randint(1, 100)
   # print('=> ' + str(random_num)) # Строчка что б проверить побыстрее. Не должна входить в программу
    enter_num = input_num()

    while True:
        if random_num == enter_num:
            print('Поздравляю. вы победили=)')
            return
        if random_num != enter_num:
            err_try -= 1

            if enter_num < random_num:
                print('Ваше число меньше загаданного.')
            else:
                print('Ваше число больше загаданного.')
            print(' Осталось {} попыток.'.format(err_try))
            enter_num = input_num()

        if err_try < 2:
            print('Попытки закончились =(')
            return


if __name__ == '__main__':
    main()
