defmodule FirsApp do
  @moduledoc """
  Documentation for FirsApp.
  """

  @doc """
  Hello world.

  ## Examples

      iex> FirsApp.hello()
      :world

  """
  def hello do
    :world
  end
end
